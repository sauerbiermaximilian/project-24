document.getElementById('form').addEventListener('submit', e => {
    // stops default behaviour of a form when submitted
    e.preventDefault();

    let success = document.getElementById('success');
    let successMsg = document.getElementById('successMsg');

    let discord = document.getElementById('discord').value;
    let opinion = document.getElementById('opinion').value;

    console.log(discord)
    console.log(opinion)
});

function checkInput() {
    let error = document.getElementById('error');
    let errorMsg = document.getElementById('errorMsg');

    let discord = document.getElementById('discord').value;
    let opinion = document.getElementById('opinion').value;

    error.classList.add('hidden');
    errorMsg.innerHTML = '';

    if (!discord.length && !opinion.length) {
        error.classList.remove('hidden');
        errorMsg.innerHTML = 'Please fill out the form!';
    }
}

let stars = document.querySelectorAll(".reviewStars span");

stars.forEach(item => {
    item.addEventListener('click', function () {
        let rating = this.getAttribute("data-rating");
        return SetRatingStar(rating, stars);
    });
});

function SetRatingStar(rating, stars) {
    let len = stars.length;
    console.log(rating);

    for (let i = 0; i < len; i++) {
        if (i < rating) {
            stars[i].innerHTML = '<i class="fas fa-star text-yellow-500"></i>';
        } else {
            stars[i].innerHTML = '<i class="far fa-star text-yellow-500"></i>';
        }
    }
}